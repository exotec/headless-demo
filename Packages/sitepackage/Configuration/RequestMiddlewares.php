<?php
return [
    'frontend' => [
        'sitepackage/preview' => [
            'after' => [
                'headless/cms-frontend/prepare-user-int'
            ],
            'target' => Exotec\Sitepackage\Middleware\Preview::class,
        ]
    ]
];
