import { createWebHistory, createRouter } from "vue-router";
import Index from "../views/Index.vue";

const routes = [
    {
        path: '/:pathMatch(.*)*',
        component: Index,
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
