<?php

namespace Exotec\Sitepackage\ViewHelpers;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

final class AssetsViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    protected $escapeOutput = false;

    public function initializeArguments()
    {
        $this->registerArgument('path', 'string', 'The path to resolve the asset files', true);
    }

    public static function renderStatic(
        array                     $arguments,
        \Closure                  $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    )
    {
        // get JS and CSS files in folder
        $path = $_SERVER['DOCUMENT_ROOT'] . $arguments['path'];
        $files = \TYPO3\CMS\Core\Utility\GeneralUtility::getFilesInDir($path, 'js,css', true, 1);
        // render script tag for js files and link tag for css files with relative path
        $output = '';
        foreach ($files as $file) {
            $fileInfo = pathinfo($file);
            if ($fileInfo['extension'] === 'js') {
                $output .= '<script src="' . $arguments['path'] . '/' . $fileInfo['filename'] . '.' . $fileInfo['extension'] . '"></script>' . PHP_EOL;
            } elseif ($fileInfo['extension'] === 'css') {
                $output .= '<link rel="stylesheet" href="' . $arguments['path'] . '/' . $fileInfo['filename'] . '.' . $fileInfo['extension'] . '">';
            }
        }

        return $output;
//        // get files in folder
//        $path = $_SERVER['DOCUMENT_ROOT'] . $arguments['path'];
//        $files = scandir($path);
//        $files = array_diff($files, array('.', '..'));
//        $files = array_values($files);
//
//        // render script tag for js files and link tag for css files with relative path
//        $output = '';
//        foreach ($files as $file) {
//            $fileInfo = pathinfo($file);
//            if ($fileInfo['extension'] === 'js') {
//                $output .= '<script src="' . $arguments['path'] . '/' . $file . '"></script>' . PHP_EOL;
//            } elseif ($fileInfo['extension'] === 'css') {
//                $output .= '<link rel="stylesheet" href="' . $arguments['path'] . '/' . $file . '">';
//            }
//        }
//
//
//        return $output;
    }
}
