<?php
declare(strict_types = 1);
namespace Exotec\Sitepackage\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Middleware to detect "preview mode" so that a hidden language is shown in the frontend
 */
class Preview implements MiddlewareInterface
{
    /**
     * @var Context
     */
    protected $context;

    public function __construct(Context $context = null)
    {
        $this->context = $context ?? GeneralUtility::makeInstance(Context::class);
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($_SERVER['HTTP_REFERER'] && $_SERVER['DOCUMENT_ROOT'] == '/var/www/html/Backend/public') {
            $referrerArr = explode('/', $_SERVER['HTTP_REFERER']);
            if ( $referrerArr[2] == 'backend-demo.ddev.site') {
                header("Location: https://frontend-demo.ddev.site".$_SERVER['REQUEST_URI']);
                exit;
            }
        }

        return $handler->handle($request);
    }

    
}
